#use wml::debian::template title="데비안 &ldquo;bullseye&rdquo; 릴리스 정보"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="552f9232b5d467c677963540a5a4236c824e87b9" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.

<if-stable-release release="bullseye">

<p>데비안 <current_release_bullseye> 가
 <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>에 나옴.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 was initially released on <:=spokendate('XXXXXXXX'):>."
/>
이 릴리스는 많은 주 변화를 포함하며,
<a href="$(HOME)/News/XXXX/XXXXXXXX">press release</a> 및
<a href="releasenotes">Release Notes</a>에 설명합니다.</p>

<p>To obtain and install Debian, see
the <a href="debian-installer/">installation information</a> page and the
<a href="installmanual">Installation Guide</a>. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

<p>The following computer architectures are supported in this release:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
<a href="reportingbugs">report other issues</a> to us.</p>

<p>Last but not least, we have a list of <a href="credits">people who take
credit</a> for making this release happen.</p>
</if-stable-release>

<if-stable-release release="buster">

<p><a
href="../buster/">buster</a> 다음안 릴리스는 코드명이 <q>bullseye</q>입니다.</p>

<p>이 릴리스는 buster 복사로 시작하고, 현재 상태는 <q><a href="$(DOC)/manuals/debian-faq/ch-ftparchives#s-testing">testing</a></q>.
This means that things should not break as badly as in unstable or
experimental distributions, because packages are allowed to enter this
distribution only after a certain period of time has passed, and when they
don't have any release-critical bugs filed against them.</p>

<p>Please note that security updates for <q>testing</q> distribution are
<strong>not</strong> yet managed by the security team. Hence, <q>testing</q> does
<strong>not</strong> get security updates in a timely manner.
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
You are encouraged to switch your
sources.list entries from testing to buster for the time being if you
need security support. See also the entry in the
<a href="$(HOME)/security/faq#testing">Security Team's FAQ</a> for
the <q>testing</q> distribution.</p>

<p>There may be a <a href="releasenotes">draft of the release notes available</a>.
Please also <a href="https://bugs.debian.org/release-notes">check the
proposed additions to the release notes</a>.</p>

<p>For installation images and documentation about how to install <q>testing</q>,
see <a href="$(HOME)/devel/debian-installer/">the Debian-Installer page</a>.</p>

<p>To find out more about how the <q>testing</q> distribution works, check
<a href="$(HOME)/devel/testing">the developers' information about it</a>.</p>

<p>People often ask if there is a single release <q>progress meter</q>.
Unfortunately there isn't one, but we can refer you to several places
that describe things needed to be dealt with for the release to happen:</p>

<ul>
  <li><a href="https://release.debian.org/">Generic release status page</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Release-critical bugs</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Base system bugs</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bugs in standard and task packages</a></li>
</ul>

<p>In addition, general status reports are posted by the release manager
to the <a href="https://lists.debian.org/debian-devel-announce/">\
debian-devel-announce mailing list</a>.</p>

</if-stable-release>
