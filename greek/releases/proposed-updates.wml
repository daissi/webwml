#use wml::debian::template title="Ο μηχανισμός των <q>proposed-updates</q> (προτεινόμενων αναβαθμίσεων)"
#include "$(ENGLISHDIR)/releases/info"

<p>Όλες οι αλλαγές στην <q>σταθερή (stable)</q> (και την προηγούμενη σταθερή 
<q>oldstable</q>) διανομή που έχουν κυκλοφορήσει περνάνε από μια εκτεταμένη 
περίοδο δοκιμασίας πριν γίνουν δεκτές στην αρχειοθήκη. Κάθε τέτοια 
επικαιροποίηση της σταθερής (και της oldstable) έκδοσης λέγεται <q>"σημειακή" 
(point) έκδοση</q>.</p>

<p>Η προετοιμασία των "σημειακών" εκδόσεων γίνεται μέσω του 
μηχανισμού των <q>proposed-updates (προτεινόμενων αναβαθμίσεων)</q>. 
Επικαιροποιημένα πακέτα ανεβαίνουν σε μια ξεχωριστή ουρά που λέγεται 
<code>p-u-new</code> (ή <code>o-p-u-new</code>), πριν γίνουν δεκτά στις
<q>proposed-updates</q> (ή <q>oldstable-proposed-updates</q>).
</p>

<p>Για να χρησιμοποιήσετε τα πακέτα αυτά με το APT, μπορείτε να προσθέσετε τις 
ακόλουθες γραμμές στο αρχείο σας
<code>sources.list</code>:</p>

<pre>
  \# proposed updates for the next point release
  deb http://ftp.us.debian.org/debian <current_release_name>-proposed-updates main contrib non-free
</pre>

<p>Σημειώστε ότι οι  <a href="$(HOME)/mirror/list">καθρέφτες του /debian/</a> 
περιέχουν αυτά τα πακέτα, οπότε δεν είναι αναγκαίο να χρησιμοποιήσετε αυτόν 
τον συγκεκριμένο, η επιλογή του ftp.us.debian.org είναι καθαρά ένα 
παράδειγμα.</p>

<p>Νέα πακέτα μπορεί να φτάσουν στις προτεινόμενες αναβαθμίσεις 
(proposed-updates) όταν οι προγραμματιστές του Debian τις ανεβάσουν είτε στο 
<q>proposed-updates</q> (<q>oldstable-proposed-updates</q>), είτε στο 
<q>stable</q> (<q>oldstable</q>). 
Η <a href="$(HOME)/doc/manuals/developers-reference/pkgs.html#upload-stable">\
διαδικασία του ανεβάσματος</a>  περιγράφεται στο Βιβλίο Αναφοράς του 
Προγραμματιστή (Developer’s Reference).
</p>

<p>Θα πρέπει να σημειωθεί ότι τα πακέτα από το 
<a href="$(HOME)/security/">security.debian.org</a> αντιγράφονται στον κατάλογο
p-u-new (o-p-u-new) αυτόματα. Την ίδια στιγμή, πακέτα που ανεβαίνουν απευθείας 
στο proposed-updates (oldstable-proposed-updates) δεν παρακολουθούνται από την 
ομάδα ασφαλείας του Debian.</p>

<p>Η τρέχουσα λίστα των πακέτων που είναι στην ουρά p-u-new (o-p-u-new) μπορεί 
να ειδωθεί στον σύνδεσμο <url 
"https://release.debian.org/proposed-updates/stable.html">
(<url "https://release.debian.org/proposed-updates/oldstable.html">).</p>
