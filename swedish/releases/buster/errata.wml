#use wml::debian::template title="Debian 10 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="a2f8d60da88f48779f320242a9af620fab33ce16"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Kända problem</toc-add-entry>
<toc-add-entry name="security">Säkerhetsproblem</toc-add-entry>

<p>Debians säkerhetsgrupp ger ut uppdateringar till paket i den stabila utgåvan
där dom har identifierat problem relaterade till säkerhet. Vänligen se
<a href="$(HOME)/security/">säkerhetssidorna</a> för information om
potentiella säkerhetsproblem som har identifierats i <q>Buster</q>.</p>

<p>Om du använder APT, lägg till följande rad i <tt>/etc/apt/sources.list</tt>
för att få åtkomst till de senaste säkerhetsuppdateringarna:</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Punktutgåvor</toc-add-entry>

<p>Ibland, när det gäller kritiska problem eller säkerhetsuppdateringar,
uppdateras utgåvedistributionen. Generellt indikeras detta som punktutgåvor.
</p>

<ul>
  <li>Den första punktutgåvan 10.1, släpptes
      <a href="$(HOME)/News/2019/20190907">den 7 september 2019</a>.</li>
  <li>Den andra punktutgåvan 10.2, släpptes
      <a href="$(HOME)/News/2019/20191116">den 16 november 2019</a>.</li>
  <li>Den tredje punktutgåvan 10.3, släpptes
      <a href="$(HOME)/News/2020/20200208">den 8 februari 2020</a>.</li>
</ul>

<ifeq <current_release_buster> 10.0 "

<p>Det finns inga punktutgåvor för Debian 10 än.</p>" "


<p>Se <a
href=http://http.us.debian.org/debian/dists/buster/ChangeLog>\
förändringsloggen</a>
för detaljer om förändringar mellan 10 och <current_release_buster/>.</p>"/>

<p>Rättningar till den utgivna stabila utgåvan går ofta genom en
utökad testningsperiod innan de accepteras i arkivet. Dock, så finns dessa
rättningar tillgängliga i mappen
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> i alla Debianarkivsspeglingar.</p>

<p>Om du använder APT för att uppdatera dina paket kan du installera de
föreslagna uppdateringarna genom att lägga till följande rad i
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# föreslagna tillägg för en punktutgåva till Debian 10
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installationssystem</toc-add-entry>

<p>
För mer information om kända problem och uppdateringar till
installationssystemet, se
<a href="debian-installer/">debian-installer</a>-sidan.
</p>
