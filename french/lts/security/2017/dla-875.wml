#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans PHP (un acronyme récursif pour
PHP : Hypertext Preprocessor), un langage de script généraliste au source libre
couramment utilisé, particulièrement adapté pour le développement web et pouvant
être intégré dans du HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7478">CVE-2016-7478</a>

<p>Zend/zend_exceptions.c dans PHP permet à des attaquants distants de provoquer
un déni de service (boucle infinie) à l'aide d'un objet Exception contrefait
dans des données sérialisées, un problème relatif à
<a href="https://security-tracker.debian.org/tracker/CVE-2015-8876">CVE-2015-8876</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7479">CVE-2016-7479</a>

<p>Lors du processus de désérialisation, le redimensionnement de la table de
hachage <q>properties</q> d’un objet sérialisé pourrait conduire à une
utilisation de mémoire après libération. Un attaquant distant peut exploiter ce
bogue pour obtenir la possibilité d’exécuter du code arbitraire. Bien que le
problème de table de propriétés affecte seulement PHP 7, cette modification
évite aussi une large gamme d’autres attaques basées sur __wakeup().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7272">CVE-2017-7272</a>

<p>La fonction fsockopen() utilise le numéro de port qui est défini dans le nom
d’hôte plutôt que le numéro passé dans le second paramètre de la fonction. Ce
dérèglement peut introduire un autre vecteur d’attaque pour une vulnérabilité
d’application déjà connue (par exemple, Serveur Side Request Forgery).</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 5.4.45-0+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-875.data"
# $Id: $
