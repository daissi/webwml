#use wml::debian::translation-check translation="a419bf7a902585eb074afc7f74774a1f7f705e0a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les CVE suivants ont été signalés envers phpmyadmin.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10802">CVE-2020-10802</a>

<p>Dans phpMyAdmin 4.x avant 4.9.5, une vulnérabilité d’injection SQL a été
découverte quand certains paramètres ne sont pas correctement protégés lors de la
création de requêtes pour des actions de recherche dans
libraries/classes/Controllers/Table/TableSearchController.php.
Un attaquant peut générer une base de données ou un nom de table contrefaits.
L’attaque peut être réalisée si un utilisateur tente certaines opérations de
recherche dans la base de données ou la table malveillante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10803">CVE-2020-10803</a>

<p>Dans phpMyAdmin 4.x avant 4.9.5, une vulnérabilité d’injection SQL
a été découverte où du code malveillant pourrait être utilisé pour déclencher
une attaque XSS jusqu’à récupérer et afficher des résultats (dans
tbl_get_field.php et libraries/classes/Display/Results.php).
L’attaquant doit être capable d’insérer des données contrefaites dans certaines
tables de base de données, ce qui, lorsque récupérées (par exemple, à travers la table
Browse), permet de déclencher une attaque XSS.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4:4.2.12-2+deb8u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpmyadmin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2154.data"
# $Id: $
