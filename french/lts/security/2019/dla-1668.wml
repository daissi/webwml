#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Fuzzing a trouvé deux problèmes supplémentaires spécifiques au format de
fichier dans libarchive, une erreur de segmentation en lecture seule dans 7z et
une boucle infinie dans ISO9660.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1000019">CVE-2019-1000019</a>

<p>Vulnérabilité de lecture hors limites dans la décompression 7zip, pouvant
provoquer un plantage (déni de service, CWE-125).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1000020">CVE-2019-1000020</a>

<p>Vulnérabilité dans l’analyseur ISO9660 pouvant provoquer un déni de service
par boucle infinie (CWE-835).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 3.1.2-11+deb8u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libarchive.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1668.data"
# $Id: $
