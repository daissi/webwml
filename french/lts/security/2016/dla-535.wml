#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Brandon Perry a découvert que xerces-c, une bibliothèque d'analyse et de
validation de XML pour C++, échoue à analyser un DTD profondément imbriqué,
provoquant un dépassement de pile. Un attaquant distant non authentifié
peut tirer avantage de ce défaut pour provoquer un déni de service à
l'encontre des applications qui utilisent la bibliothèque xerces-c.</p>

<p>En complément, cette mise à jour inclut une amélioration pour permettre
aux applications de désactiver complètement le traitement de DTD grâce à
l'usage d'une variable d'environnement (XERCES_DISABLE_DTD).</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.1.1-3+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xerces-c.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-535.data"
# $Id: $
