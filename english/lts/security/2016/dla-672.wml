<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2848">CVE-2016-2848</a>

      <p>A server vulnerable to this defect can be forced to
      exit with an assertion failure if it receives a malformed packet.
      Authoritative and recursive servers are both vulnerable.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:9.8.4.dfsg.P1-6+nmu2+deb7u12.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-672.data"
# $Id: $
