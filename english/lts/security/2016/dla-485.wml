<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This security update fixes a security issue in extplorer. We recommend you
upgrade your extplorer package.</p>

<ul>

    <li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5660">CVE-2015-5660</a>
      <p>Cross-site request forgery (CSRF) vulnerability allows remote
      attackers to hijack the authentication of arbitrary users for
      requests that execute PHP code.</p></li>

</ul>

<p>Further information about Debian LTS security Advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in extplorer version 2.1.0b6+dfsg.3-4+deb7u3</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-485.data"
# $Id: $
