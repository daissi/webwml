<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Markus Krell discovered that Xymon (formerly known as Hobbit), a
network- and applications-monitoring system, was vulnerable to the
following security issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2054">CVE-2016-2054</a>

  <p>The incorrect handling of user-supplied input in the <q>config</q>
  command can trigger a stack-based buffer overflow, resulting in
  denial of service (via application crash) or remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2055">CVE-2016-2055</a>

  <p>The incorrect handling of user-supplied input in the <q>config</q>
  command can lead to an information leak by serving sensitive
  configuration files to a remote user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2056">CVE-2016-2056</a>

  <p>The commands handling password management do not properly validate
  user-supplied input, and are thus vulnerable to shell command
  injection by a remote user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2058">CVE-2016-2058</a>

  <p>Incorrect escaping of user-supplied input in status webpages can
  be used to trigger reflected cross-site scripting attacks.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.3.0~beta2.dfsg-9.1+deb7u1.</p>

<p>We recommend that you upgrade your xymon packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-488.data"
# $Id: $
