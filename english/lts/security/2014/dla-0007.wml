<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3153">CVE-2014-3153</a>:

<p>Pinkie Pie discovered an issue in the futex subsystem that
allows a local user to gain ring 0 control via the futex syscall.  An
unprivileged user could use this flaw to crash the kernel (resulting in
denial of service) or for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-1438">CVE-2014-1438</a>:

<p>The restore_fpu_checking function in
arch/x86/include/asm/fpu-internal.h in the Linux kernel before 3.12.8 on the
AMD K7 and K8 platforms does not clear pending exceptions before proceeding
to an EMMS instruction, which allows local users to cause a denial of
service (task kill) or possibly gain privileges via a crafted application.</p></li>

</ul>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in linux-2.6 version 2.6.32-48squeeze7</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0007.data"
# $Id: $
