<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in spamassassin, a Perl-based spam
filter using text analysis.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11805">CVE-2018-11805</a>

    <p>Malicious rule or configuration files, possibly downloaded from an
    updates server, could execute arbitrary commands under multiple
    scenarios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12420">CVE-2019-12420</a>

    <p>Specially crafted mulitpart messages can cause spamassassin to use
    excessive resources, resulting in a denial of service.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 3.4.2-1~deb9u2.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 3.4.2-1+deb10u1.</p>

<p>We recommend that you upgrade your spamassassin packages.</p>

<p>For the detailed security status of spamassassin please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/spamassassin">\
https://security-tracker.debian.org/tracker/spamassassin</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4584.data"
# $Id: $
