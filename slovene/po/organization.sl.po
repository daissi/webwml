msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:10+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "trenutni"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "cxlan"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:34
#, fuzzy
msgid "Stable Release Manager"
msgstr "Upravitelj izdaje"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
#, fuzzy
msgid "chair"
msgstr "predsedujocxi"

#: ../../english/intro/organization.data:41
#, fuzzy
msgid "assistant"
msgstr "Upravitelj izdaje"

#: ../../english/intro/organization.data:43
#, fuzzy
msgid "secretary"
msgstr "Tajnik"

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:47
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:75
msgid "Officers"
msgstr "Funkcionarji"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:99
msgid "Distribution"
msgstr "Distribucija"

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:235
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:238
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:242
#, fuzzy
msgid "Publicity team"
msgstr "Odnosi z javnostjo"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr "Podpora in infrastruktura"

#: ../../english/intro/organization.data:78
msgid "Leader"
msgstr "Vodja"

#: ../../english/intro/organization.data:80
msgid "Technical Committee"
msgstr "Tehnicxna komisija"

#: ../../english/intro/organization.data:94
msgid "Secretary"
msgstr "Tajnik"

#: ../../english/intro/organization.data:102
msgid "Development Projects"
msgstr "Razvojni projekti"

#: ../../english/intro/organization.data:103
msgid "FTP Archives"
msgstr "Arhive FTP"

#: ../../english/intro/organization.data:105
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "FTP Assistants"
msgstr ""

#: ../../english/intro/organization.data:116
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:120
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:122
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:126
msgid "Release Management"
msgstr "Upravljanje izdaje"

#: ../../english/intro/organization.data:128
#, fuzzy
msgid "Release Team"
msgstr "Upravitelj izdaje"

#: ../../english/intro/organization.data:141
msgid "Quality Assurance"
msgstr "Zagotavljanje kakovosti"

#: ../../english/intro/organization.data:142
msgid "Installation System Team"
msgstr "Skupina za namestitveni sistem"

#: ../../english/intro/organization.data:143
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:144
#, fuzzy
msgid "Release Notes"
msgstr "Upravitelj izdaje"

#: ../../english/intro/organization.data:146
msgid "CD Images"
msgstr "CD Images"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "Produkcija"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "Testiranje"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:162
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "Podpora in infrastruktura"

#: ../../english/intro/organization.data:164
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:171
#, fuzzy
msgid "Buildd administration"
msgstr "Sistemska administracija"

#: ../../english/intro/organization.data:189
msgid "Documentation"
msgstr "Dokumentacija"

#: ../../english/intro/organization.data:194
#, fuzzy
msgid "Work-Needing and Prospective Packages list"
msgstr "Seznam nadebudnih in dela potrebnih paketov"

#: ../../english/intro/organization.data:196
msgid "Ports"
msgstr "Druge platforme"

#: ../../english/intro/organization.data:226
msgid "Special Configurations"
msgstr "Posebne konfiguracije"

#: ../../english/intro/organization.data:228
msgid "Laptops"
msgstr "Prenosni racxunalniki"

#: ../../english/intro/organization.data:229
msgid "Firewalls"
msgstr "Pozxarni zidovi"

#: ../../english/intro/organization.data:230
msgid "Embedded systems"
msgstr ""

#: ../../english/intro/organization.data:245
msgid "Press Contact"
msgstr "Predstavnik za tisk"

#: ../../english/intro/organization.data:247
msgid "Web Pages"
msgstr "Spletne strani"

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:269
#, fuzzy
msgid "Debian Women Project"
msgstr "Razvojni projekti"

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr "Dogodki"

#: ../../english/intro/organization.data:289
#, fuzzy
msgid "DebConf Committee"
msgstr "Tehnicxna komisija"

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr "Partnerski program"

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:329
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:332
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr "Podpora uporabnikov"

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr "Sledilnik napak"

#: ../../english/intro/organization.data:414
#, fuzzy
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administracija posxtnih seznamov"

#: ../../english/intro/organization.data:422
#, fuzzy
msgid "New Members Front Desk"
msgstr "Sprejemnica za nove vzdrzxevalce"

#: ../../english/intro/organization.data:428
#, fuzzy
msgid "Debian Account Managers"
msgstr "Upravitelji uporabnisxkih imen razvijalcev"

#: ../../english/intro/organization.data:432
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Vzdrzxevalci kljucxev (PGP in GPG)"

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr "Skupina za varnost"

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr "Svetovalska stran"

#: ../../english/intro/organization.data:453
#, fuzzy
msgid "CD Vendors Page"
msgstr "Prodajalci"

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr "Pravilnik"

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr "Sistemska administracija"

#: ../../english/intro/organization.data:460
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Na ta naslov, lahko posxljete sporocxilo, cxe naletite na tezxavo na katerem "
"od Debianovih racxunalnikov, vkljucxno s tezxavami z gesli in paketi, ki jih "
"potrebujete."

#: ../../english/intro/organization.data:469
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Cxe imate strojne tezxave z Debianovimi racxunalniki, glejte stran <a href="
"\"https://db.debian.org/machines.cgi\">Debianovi racxunalniki</a>, kjer "
"boste nasxli podatke o administratorjih posameznih racxunalnikov."

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr "Skrbnik imenika razvijalcev LDAP"

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr "Zrcalni strezxniki"

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr "Vzdrzxevalec DNSa"

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr "Sledilnikov paketov"

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:488
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:491
#, fuzzy
msgid "Salsa administrators"
msgstr "Sistemska administracija"

#~ msgid "Individual Packages"
#~ msgstr "Posamezni paketi"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Skupina za namestitveni sistem"

#~ msgid "Publicity"
#~ msgstr "Odnosi z javnostjo"

#, fuzzy
#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribucija"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "Upravitelj izdaje ''stable''"

#~ msgid "Vendors"
#~ msgstr "Prodajalci"

#~ msgid "APT Team"
#~ msgstr "Skupina za APT"

#~ msgid "Mailing List Archives"
#~ msgstr "Arhive posxtnih seznamov"

#, fuzzy
#~ msgid "Security Testing Team"
#~ msgstr "Skupina za varnost"

#~ msgid "Mailing list"
#~ msgstr "Posxtni seznam"

#~ msgid "Installation"
#~ msgstr "Namestitev"

#~ msgid "Delegates"
#~ msgstr "Delegati"

#, fuzzy
#~ msgid "Installation System for ``stable''"
#~ msgstr "Skupina za namestitveni sistem"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Skupina za varnost"

#, fuzzy
#~ msgid "Alioth administrators"
#~ msgstr "Sistemska administracija"
