#use wml::debian::template title="Debian Med"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="b9967d1f4d930716e9646a92bda776ced3c44cce"

<h2>Beschrijving van het project</h2>

<p>Debian Med is een
   "<a href="https://blends.debian.org/blends/">Debian Pure Blend</a>"
   (doelgroepspecifieke uitgave van Debian) dat tot doel heeft Debian
   te ontwikkelen tot een besturingssysteem dat bijzonder goed aangepast
   is aan de vereisten van de medische praktijk en van het biomedisch
   onderzoek.
   Debian Med wil een volledig vrij en open systeem zijn voor alle taken
   op het gebied van medische zorg en onderzoek. Om dit doel te bereiken
   integreert Debian Med in het Debian OS vrije en openbronsoftware die
   verband houdt met medische beeldvorming, bio-informatica,
   IT-infrastructuur van ziekenhuizen en andere verwante zaken.
</p>


<p>Debian Med bevat een reeks metapakketten die aangeven welke andere
   Debian pakketten vereist zijn, en op die manier wordt het volledige
   systeem klaargemaakt om specifieke taken te vervullen.
   Het beste overzicht over Debian Med is te vinden op de
   <a href="https://blends.debian.org/med/tasks/">takenpagina</a>.
</p>

<p>Voor een meer diepgaande kennismaking staan er
   <a href="https://people.debian.org/~tille/talks/">verschillende
   voordrachten over Debian Med en over Debian Pure Blends in het
   algemeen</a> ter beschikking onder de vorm van presentaties, en sommige
   ervan werden op video opgenomen.
</p>


<h2>Contact- en ontwikkelaarsinformatie</h2>

<p>
De <a href="mailto:debian-med@lists.debian.org">mailinglijst Debian Med</a>
is het centrale communicatiepunt voor Debian Med. Deze fungeert als
forum voor potentiële en actuele gebruikers van het Debian systeem, die
hun computer voor medische taken wensen te gebruiken. Daarnaast wordt
de lijst ook gebruikt om de ontwikkelingsactiviteit met betrekking
tot de verschillende geneeskundige thema's te coördineren. U kunt
zich bij de lijst aanmelden en afmelden via de
<a href="https://lists.debian.org/debian-med/">webpagina van de
mailinglijst</a>.
Op die pagina vindt u ook de mailinglijstarchieven.
</p>
<p>
Belangrijke plaatsen met informatie voor ontwikkelaars:
</p>
<ol>
  <li><a href="https://wiki.debian.org/DebianMed">De Wikipagina</a></li>
  <li><a href="https://blends.debian.org/med/">De Blendspagina</a></li>
  <li>De <a href="https://med-team.pages.debian.net/policy/">Debian Med beleidsrichtlijnen</a> waar uitgelegd wordt welke verpakkingsregels het team hanteert</li>
  <li><a href="https://salsa.debian.org/med-team/">Git-depot van Debian Med pakketten op Salsa</a></li>
</ol>

<h2>Inbegrepen softwareprojecten</h2>

Het <a href="https://blends.debian.org/blends">Pure Blends</a>-systeem
van Debian geeft een automatisch gegenereerd overzicht van alle in een
Blend vervatte software. Bekijk de zogenaamde
<b><a href="https://blends.debian.org/med/tasks/">takenpagina's van
Debian Med</a></b> om te weten welke software opgenomen is en welke
projecten nog op onze to-dolijst staan voor opname in Debian.


<h2>Projectdoelstellingen</h2>

<ul>
  <li>Een degelijke softwarebasis voor medische zorg uitbouwen met
      klemtoon op makkelijke installatie, eenvoudig onderhoud en
      veiligheid.</li>
  <li>Samenwerking tussen auteurs van verschillende softwareprojecten
      met vergelijkbare doelstellingen, aanmoedigen.</li>
  <li>Testsuite voor een eenvoudige evaluatie van de kwaliteit van
      medische software.</li>
  <li>Informatie en documentatie over medische software verschaffen.</li>
  <li>Bovenstroomse auteurs helpen om hun producten voor Debian verpakt
      te krijgen.</li>
  <li>Commerciële softwarebedrijven de kracht tonen van een degelijk
      basissysteem en hen ertoe aanzetten erover te denken om hun
      software bruikbaar te maken voor Linux of zelfs om op openbroncode
      over te schakelen.</li>
</ul>


<h2>Waarmee kan ik helpen?</h2>

<p>
   Er bestaat een <a href="https://wiki.debian.org/DebianMedTodo">Wikipagina met een lijst zaken</a> die men kan doen
om het project te helpen.
</p>

<h2>Marketing &amp; PR</h2>

<p>Zodra we voor dit project iets hebben dat toonbaar is, en zelfs in
   de ontwikkelingsfasen van dit project, zullen de ogen van de wereld
   op ons gericht zijn. We zullen noodzakelijkerwijs voor de bekendmaking
   moeten samenwerken met press@debian.org, alsook om ervoor te zorgen
   dat dit project de gewenste aandacht krijgt. Met dit doel zullen
   we een aantal presentaties maken met voordrachten over Debian Med.
</p>


<h2>Links</h2>

<ul>
  <li>Debian Med werkt nauw samen met
      <a href="http://nebc.nerc.ac.uk/tools/bio-linux/bio-linux-6.0">Bio-Linux</a>.
      Hoewel Bio-Linux gebaseerd is op LTS-releases van Ubuntu,
      worden de pakketten uit het vakgebied van de biologie
      binnen Debian onderhouden door het team van Debian Med.
  </li>
</ul>
