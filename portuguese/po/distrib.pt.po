# Brazilian Portuguese translation for Debian website distrib.pot
# Copyright (C) 2004-2017 Software in the Public Interest, Inc.
#
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2007
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian WebWML\n"
"PO-Revision-Date: 2017-02-28 21:52-0300\n"
"Last-Translator: Marcelo Gomes de Santana <marcelo@msantana.eng.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/distrib/search_packages-form.inc:8
#: ../../english/distrib/search_contents-form.inc:9
msgid "Keyword"
msgstr "Palavra-Chave"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Pesquisar em"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Somente o nome dos pacotes"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Descrições"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Nomes dos pacotes fonte"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Exibir somente combinações exatas"

#: ../../english/distrib/search_packages-form.inc:25
#: ../../english/distrib/search_contents-form.inc:25
msgid "Distribution"
msgstr "Distribuição"

#: ../../english/distrib/search_packages-form.inc:27
#: ../../english/distrib/search_contents-form.inc:27
msgid "experimental"
msgstr "experimental"

#: ../../english/distrib/search_packages-form.inc:28
#: ../../english/distrib/search_contents-form.inc:28
msgid "unstable"
msgstr "instável"

#: ../../english/distrib/search_packages-form.inc:29
#: ../../english/distrib/search_contents-form.inc:29
msgid "testing"
msgstr "testing"

#: ../../english/distrib/search_packages-form.inc:30
#: ../../english/distrib/search_contents-form.inc:30
msgid "stable"
msgstr "estável"

#: ../../english/distrib/search_packages-form.inc:31
#: ../../english/distrib/search_contents-form.inc:31
msgid "oldstable"
msgstr "oldstable"

#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
#: ../../english/distrib/search_contents-form.inc:38
msgid "any"
msgstr "qualquer (any)"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Seção"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main (principal)"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "não-livre (non-free)"

#: ../../english/distrib/search_packages-form.inc:43
#: ../../english/distrib/search_contents-form.inc:48
msgid "Search"
msgstr "Pesquisar"

#: ../../english/distrib/search_packages-form.inc:44
#: ../../english/distrib/search_contents-form.inc:49
msgid "Reset"
msgstr "Reset"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Mostrar"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "caminhos terminando com a palavra chave"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "pacotes que contêm arquivos com este nome"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "pacotes que contêm arquivos cujos nomes contêm a palavra chave"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Arquitetura"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "PC 64 bits (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "ARM 64 bits (amd64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "EABI ARM (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "ARM com unidade de ponto flutuante (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "PC 32 bits Hurd (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "PC 32 bits (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "PC 32 bits KFreeBSD (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "PC 64 bits KFreeBSD (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (big endian)"

#: ../../english/releases/arches.data:22
msgid "64-bit MIPS (little endian)"
msgstr "MIPS 64 bits (little endian)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr "Processadores POWER"

#: ../../english/releases/arches.data:26
#, fuzzy
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "MIPS 64 bits (little endian)"

#: ../../english/releases/arches.data:27
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:28
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:29
msgid "SPARC"
msgstr "SPARC"

#~ msgid "Intel x86"
#~ msgstr "Intel x86"

#~ msgid "AMD64"
#~ msgstr "AMD64"

#~ msgid "HP PA/RISC"
#~ msgstr "HP PA/RISC"

#~ msgid "Intel IA-64"
#~ msgstr "Intel IA-64"

#~ msgid "kFreeBSD (AMD64)"
#~ msgstr "kFreeBSD (AMD64)"

#~ msgid "kFreeBSD (Intel x86)"
#~ msgstr "KFreeBSD (Intel x86)"

#~ msgid "Hurd (i386)"
#~ msgstr "Hurd (i386)"

#~ msgid "MIPS"
#~ msgstr "MIPS"

#~ msgid "MIPS (DEC)"
#~ msgstr "MIPS (DEC)"

#~ msgid "non-US"
#~ msgstr "não-US"

#~ msgid "Search case sensitive"
#~ msgstr "Pesquisa diferenciando maiúsculas e minúsculas"

#~ msgid "Allow searching on subwords"
#~ msgstr "Permitir pesquisa em sub-palavras"

#~ msgid "yes"
#~ msgstr "sim"

#~ msgid "no"
#~ msgstr "não"

#~ msgid "Case sensitive"
#~ msgstr "Diferenciar maiúsculas e minúsculas"

#~ msgid "all files in this package"
#~ msgstr "todos os arquivos neste pacote"

#~ msgid "packages that contain files or directories named like this"
#~ msgstr "pacotes que contêm arquivos ou diretórios com este nome"
